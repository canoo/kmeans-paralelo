for (( i = 4; i < 17; i=i*2 )) 
do 
	#nodos=$(( i < 4 ? i : 4 ))
	echo "#!/bin/bash" > ejecucion.sh
	echo "#SBATCH -J kmeans${i}_fuerte_openmp" >> ejecucion.sh
	echo "#SBATCH -o datosEjecucionFuerte${i}.out" >> ejecucion.sh
	#echo "#SBATCH  --nodes ${nodos}" >> ejecucion.sh
	echo "#SBATCH  --ntasks ${i}" >> ejecucion.sh
	#echo "#SBATCH -t 00:05:00 ">> ejecucion.sh
	echo "time prun ./a.out input8.dat"  >> ejecucion.sh	
	sbatch ejecucion.sh
	
 done

#sleep 5s 
#sh scriptPrinteaDatos.sh
