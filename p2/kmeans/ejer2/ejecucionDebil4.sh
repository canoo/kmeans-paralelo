#!/bin/bash
#SBATCH -J kmeansD4
#SBATCH -o datosEjecucionDebil4.out
#SBATCH  --ntasks 4
#SBATCH -N 4
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=4

time prun ./a.out input4.dat
