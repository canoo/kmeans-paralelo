#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define DIM 3
//#define DEBUG 0

void redVectors (void *, void *, int *, MPI_Datatype * );

void redVectors (void* invec, void* inoutvec, int* len, MPI_Datatype* dtype)
{
    double* input = (double*)invec;
    double* output = (double*)inoutvec;

    for (int h=0; h< *(len); h++ )
        output[h] += input[h];
}

void redVectors2 (void *, void *, int *, MPI_Datatype * );

void redVectors2 (void* invec, void* inoutvec, int* len, MPI_Datatype* dtype)
{
    int* input = (int*)invec;
    int* output = (int*)inoutvec;

    for (int h=0; h< *(len); h++ )
        output[h] += input[h];
}


int main(int argc, char **argv)
{

	int i, j, k, n, c;
	double dmin, dx;
	double *x, *mean, *sum;//, *sumTotal;
	int *count, color;//, *countTotal;
	int flips, iter;
	int block;

	int myrank,size;
	MPI_Status status;


	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);

  MPI_Op op;
	MPI_Op_create( (MPI_User_function *)redVectors, 1, &op );

  MPI_Op op2;
  MPI_Op_create( (MPI_User_function *)redVectors2, 1, &op2 );


  /* Lectura del número de centroides y de puntos. */
	if(myrank == 0){
		scanf("%d", &k);
		scanf("%d", &n);
    block = n / size;

    x = (double *)malloc(sizeof(double)*DIM*n); //datos
    //sumTotal= (double *)malloc(sizeof(double) * DIM * k); //recepcion de cada nodo
    //countTotal = (int *)malloc(sizeof(int) * k ); //recepcion de cada nodo
  }

  //bcast de variables
	MPI_Bcast(&block,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&k,1,MPI_INT,0,MPI_COMM_WORLD);

  //Init comun
  mean = (double *) malloc (sizeof(double)* DIM * k); //centroides
  sum= (double *)malloc(sizeof(double) * DIM * k); //suma cada compenente centroide
  count = (int *)malloc(sizeof(int) * k ); // apariciones de cada

	double *xx = (double *) malloc (sizeof(double)* DIM * block); //datos partidos
	int *ccluster = (int *)malloc(sizeof(int) * block ); //cluster para cada dato partido

  for (i = 0; i<block; i++)
    ccluster[i] = 0;

  /*
	 * Lectura de los datos del fichero de entrada.
	 * El fichero se debe pasar redireccionando la entrada estándar.
	 */
  if(myrank == 0){
		for (i = 0; i<k; i++)
			scanf("%lf %lf %lf", mean+i*DIM, mean+i*DIM+1, mean+i*DIM+2);
		for (i = 0; i<n; i++)
			scanf("%lf %lf %lf", x+i*DIM, x+i*DIM+1, x+i*DIM+2);
  }

	//Reparticion de datos
	MPI_Scatter(x,block*DIM,MPI_DOUBLE,xx,block*DIM,MPI_DOUBLE,0,MPI_COMM_WORLD);

  /*for(i = 0 ; i< block;i++){
    printf("myrank %d: %2f ",myrank, xx[i] );
    printf("myrank %d: %2f ",myrank, xx[i+1] );
    printf("myrank %d: %2f \n",myrank, xx[i+2] );
  }*/


/* Bucle principal de K-Means */
	flips = 1;
  iter=0;

	while (flips>0) {
	//for(int aaa = 0; aaa<100 ; aaa++){

		flips = 0;
    iter++;

		//reset vectores
		for (j = 0; j < k; j++) {
			count[j] = 0;
			for (i = 0; i < DIM; i++)
				sum[j*DIM+i] = 0.0;
		}

		//Centroides
		MPI_Bcast(mean,k * DIM,MPI_DOUBLE,0,MPI_COMM_WORLD);

	  //Bucle principal
		for (i = 0; i < block; i++) {

			dmin = -1;
      color = ccluster[i];

			for (c = 0; c < k; c++) {
				dx = 0.0;

				//distancia
				for (j = 0; j < DIM; j++)
					dx +=  (xx[i*DIM+j] - mean[c*DIM+j])*(xx[i*DIM+j] - mean[c*DIM+j]);

				if (dx < dmin || dmin == -1) {
					color = c;
					dmin = dx;
				}

			}

			//check si ha cambiado el color para cambiarlo
			if (ccluster[i] != color) {
				flips++;
				ccluster[i] = color;
	     }

		}

    MPI_Allreduce(MPI_IN_PLACE,&flips,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);

		//Recalculo de centroides
		//suma componentes y cuenta clusters
    for (i = 0; i < block; i++) {
			count[ ccluster[i] ]++;
			for (j = 0; j < DIM; j++)
				sum[ ccluster[i]*DIM+j ] += xx[ i*DIM+j ];
		}


    //MPI_IN_PLACE
		//MPI_Reduce(sum,sumTotal,DIM * k,MPI_DOUBLE,op,0,MPI_COMM_WORLD);
		//MPI_Reduce(count,countTotal, k ,MPI_INT,op2,0,MPI_COMM_WORLD);
		if(myrank == 0){

      MPI_Reduce(MPI_IN_PLACE,sum,DIM * k,MPI_DOUBLE,op,0,MPI_COMM_WORLD);
  		MPI_Reduce(MPI_IN_PLACE,count, k ,MPI_INT,op2,0,MPI_COMM_WORLD);

			for (i = 0; i < k; i++) {
				for (j = 0; j < DIM; j++) {
					mean[i*DIM+j] = sum[i*DIM+j]/count[i];
					}
			}

		}else{

      MPI_Reduce(sum,sum,DIM * k,MPI_DOUBLE,op,0,MPI_COMM_WORLD);
      MPI_Reduce(count,count, k ,MPI_INT,op2,0,MPI_COMM_WORLD);

    }

	} //End of While

/*
 * Escritura de resultados por pantalla.
 * Si no se activa la variable DEBUG sólo se escriben los centroides.
 * Si se activa DEBUG se muestran todos los puntos.
 * Activar DEBUG sólo para depurar el programa.
 */

 if(myrank == 0){
   for (i = 0; i < k; i++) {
  		for (j = 0; j < DIM; j++)
  			printf("%5.2f ", mean[i*DIM+j]);
  		printf("\n");
  	}
 }


	#ifdef DEBUG
	for (i = 0; i < block; i++) {
		for (j = 0; j < DIM; j++)
			printf("%5.2f ", xx[i*DIM+j]);
		printf("%d\n", ccluster[i]);
	}
	#endif

	MPI_Op_free( &op );
  MPI_Op_free( &op2 );

	MPI_Finalize();

	return(0);
}
