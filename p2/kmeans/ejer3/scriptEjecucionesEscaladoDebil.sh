for (( i = 8; i < 17; i=i*2 )) 
do 
	#nodos=$(( i < 4 ? i : 4 ))
	echo "#!/bin/bash" > ejecucion.sh
	echo "#SBATCH -J kmeans${i}-balanceador-debil" >> ejecucion.sh
	echo "#SBATCH -o datosEjecucion${i}.out" >> ejecucion.sh
	#echo "#SBATCH  --nodes ${nodos}" >> ejecucion.sh
	echo "#SBATCH  --ntasks ${i}" >> ejecucion.sh
	cat especial.txt >> ejecucion.sh
	echo "time prun ./a.out  input${i}.dat"  >> ejecucion.sh	
	sbatch ejecucion.sh
	
 done

#sleep 5s 
#sh scriptPrinteaDatos.sh
