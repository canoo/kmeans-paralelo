#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#define DIM 3
//#define DEBUG 0

void redVectors (void *, void *, int *, MPI_Datatype * );

void redVectors (void* invec, void* inoutvec, int* len, MPI_Datatype* dtype)
{
    double* input = (double*)invec;
    double* output = (double*)inoutvec;

    for (int h=0; h< *(len); h++ )
        output[h] += input[h];
}

void redVectors2 (void *, void *, int *, MPI_Datatype * );

void redVectors2 (void* invec, void* inoutvec, int* len, MPI_Datatype* dtype)
{
    int* input = (int*)invec;
    int* output = (int*)inoutvec;

    for (int h=0; h< *(len); h++ )
        output[h] += input[h];
}


int main(int argc, char **argv)
{

   FILE * fp;

	int i, j, k, n, c;
	double dmin, dx;
	double *x, *mean, *sum;
	int *count, color;
	int flips, iter;
	int block;

  double *pCargaTotal;
  int *bloquesPorCore, *desplazamientoCore;
	int myrank,size;
	MPI_Status status;

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);

  MPI_Op op;
	MPI_Op_create( (MPI_User_function *)redVectors, 1, &op );

  MPI_Op op2;
  MPI_Op_create( (MPI_User_function *)redVectors2, 1, &op2 );


  /* Lectura del número de centroides y de puntos. */
	if(myrank == 0){

    fp = fopen ("input100.dat", "ro");
		fscanf(fp,"%d", &k);
		fscanf(fp,"%d", &n);

    printf("%d \n",n );
    //fflush(stdout
    x = (double *)malloc(sizeof(double)*DIM*n); //datos

  }

  //bcast de variables
	//MPI_Bcast(&block,1,MPI_INT,0,MPI_COMM_WORLD);

	MPI_Bcast(&k,1,MPI_INT,0,MPI_COMM_WORLD);
  MPI_Bcast(&n,1,MPI_INT,0,MPI_COMM_WORLD);
  block = (int) n / size;

  //balanceo
  pCargaTotal = (double *) malloc (sizeof(double)* size);
  bloquesPorCore = (int *) malloc (sizeof(int)* size);
  desplazamientoCore = (int *) malloc (sizeof(int)* size);

  //Init comun
  mean = (double *) malloc (sizeof(double)* DIM * k); //centroides
  sum= (double *)malloc(sizeof(double) * DIM * k); //suma cada compenente centroide
  count = (int *)malloc(sizeof(int) * k ); // apariciones de cada

	double *xx = (double *) malloc (sizeof(double)* DIM * block); //datos partidos
	int *cluster = (int *)malloc(sizeof(int) * block ); //cluster para cada dato partido

  #pragma omp parallel for private(i) shared(cluster)
  for (i = 0; i<block; i++)
    cluster[i] = 0;

  /*
	 * Lectura de los datos del fichero de entrada.
	 * El fichero se debe pasar redireccionando la entrada estándar.
	 */
  if(myrank == 0){
		for (i = 0; i<k; i++)
			fscanf(fp,"%lf %lf %lf", mean+i*DIM, mean+i*DIM+1, mean+i*DIM+2);
		for (i = 0; i<n; i++)
			fscanf(fp,"%lf %lf %lf", x+i*DIM, x+i*DIM+1, x+i*DIM+2);

  }

  MPI_Scatter(x,block*DIM,MPI_DOUBLE,xx,block*DIM,MPI_DOUBLE,0,MPI_COMM_WORLD);
  MPI_Bcast(mean,k * DIM,MPI_DOUBLE,0,MPI_COMM_WORLD);

  /*Distribución de la carga */

  double itime, ftime, exec_time;
  itime = omp_get_wtime(); //obteniendo tiempo

  #pragma omp parallel private(i,c,j,color,dmin,dx) shared(flips,mean,cluster,xx)
  {

  #pragma omp for private(i) reduction(+: flips)
  for (i = 0; i < block; i++) { //Creo que mejor es coger este bucle, depende
                                //del numero de threads y tamaño de block
    dmin = -1;
    color = cluster[i];

    for (c = 0; c < k; c++) {
      dx = 0.0;

      //distancia
      for (j = 0; j < DIM; j++)
        dx +=  (xx[i*DIM+j] - mean[c*DIM+j])*(xx[i*DIM+j] - mean[c*DIM+j]);

      if (dx < dmin || dmin == -1) {
        color = c;
        dmin = dx;
      }
    }
  }
}

//fin del parallel y reparticion
ftime = omp_get_wtime();
exec_time = ftime - itime;

//printf("time: %f \n", exec_time);
//MPI_Gather(&exec_time,1,MPI_DOUBLE,pCargaTotal,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
MPI_Allgather(&exec_time,1,MPI_DOUBLE,pCargaTotal,1,MPI_DOUBLE, MPI_COMM_WORLD);

double totalTime = 0;
for(int core = 0; core < size; core++){
  totalTime += pCargaTotal[core];
}

//printf("%f\n",totalTime );


//lo calculo con todos porque el root lo tiene que calcular
bloquesPorCore[0] = (int) (pCargaTotal[0]/totalTime * n) * DIM;
desplazamientoCore[0] = 0;
//printf("desplazamientoCore: %d, bloques: %d\n",desplazamientoCore[0], bloquesPorCore[0] );

int totalBloques = (int) (pCargaTotal[0]/totalTime * n);

#pragma omp parallel for shared(bloquesPorCore,desplazamientoCore) reduction(+: totalBloques)
for (int core = 1; core < size; core ++){
  totalBloques+= (int) (pCargaTotal[core]/totalTime * n);
  bloquesPorCore[core] = (int) (pCargaTotal[core]/totalTime * n) * DIM;
  desplazamientoCore[core] = desplazamientoCore[core-1] + bloquesPorCore[core-1];
  //printf("desplazamientoCore: %d, bloques: %d\n",desplazamientoCore[core], bloquesPorCore[core] );
}
bloquesPorCore[size-1]+= (int) (n - totalBloques) * DIM; //para tener todos

block = bloquesPorCore[myrank] / DIM; //(pCargaTotal[myrank]/totalTime * n);
xx = (double *) malloc (sizeof(double) * bloquesPorCore[myrank]);
cluster = (int *)malloc(sizeof(int) * block );


MPI_Scatterv(x,bloquesPorCore,desplazamientoCore,MPI_DOUBLE,xx,bloquesPorCore[myrank],MPI_DOUBLE,0,MPI_COMM_WORLD);

#pragma omp parallel for private(i) shared(cluster)
for (i = 0; i<block; i++)
  cluster[i] = 0;

/*for(int i = 0; i< block; i++){
    printf("myrank: %d, Fila: %f, %f, %f \n",myrank, xx[i], xx[i+1], xx[i+2]);
}*/
  //printf("myrank: %d %f \n",myrank, xx[bloquesPorCore[myrank] -1]);
/* Bucle principal de K-Means */
	flips=1;
  iter=0;

	while (flips > 0) {
  //for (int aaa = 0; aaa<10; aaa++){

		flips = 0;
    iter++;

    //Centroides
    MPI_Bcast(mean,k * DIM,MPI_DOUBLE,0,MPI_COMM_WORLD);

    /*if(myrank == 0){
      for (i = 0; i < k; i++) {
     		for (j = 0; j < DIM; j++)
     			printf("%5.2f ", mean[i*DIM+j]);
     		printf("\n");
     	}
    }*/

    //zona paralela
    #pragma omp parallel private(i,c,j,color,dmin,dx) shared(flips,mean,cluster,xx,count,sum,k,iter,block)
    {
    //lo meto dentro, asi va mas rapido
    //reset
    #pragma omp for
    for (j = 0; j < k; j++) {
			count[j] = 0;
			for (i = 0; i < DIM; i++)
				sum[j*DIM+i] = 0.0;
    }

     //Bucle principal
    #pragma omp for reduction(+: flips)
		for (i = 0; i < block; i++) { //Creo que mejor es coger este bucle, depende
                                  //del numero de threads y tamaño de block
			dmin = -1;
      color = cluster[i];

			for (c = 0; c < k; c++) {
				dx = 0.0;

				//distancia
				for (j = 0; j < DIM; j++)
					dx +=  (xx[i*DIM+j] - mean[c*DIM+j])*(xx[i*DIM+j] - mean[c*DIM+j]);

				if (dx < dmin || dmin == -1) {
					color = c;
					dmin = dx;
				}

			}
			//check si ha cambiado el color para cambiarlo
			if (cluster[i] != color) {
				flips++;
				cluster[i] = color;
	     }

		}
 } //fin del parallel


  MPI_Allreduce(MPI_IN_PLACE,&flips,1,MPI_INT,MPI_SUM,MPI_COMM_WORLD);

  //Recalculo de centroides
  //suma componentes y cuenta clusters

  for (i = 0; i < block; i++) {
    count[ cluster[i] ]++;
    for (j = 0; j < DIM; j++)
      sum[ cluster[i]*DIM+j ] += xx[ i*DIM+j ];
  }

		if(myrank == 0){

      MPI_Reduce(MPI_IN_PLACE,sum,DIM * k,MPI_DOUBLE,op,0,MPI_COMM_WORLD);
  		MPI_Reduce(MPI_IN_PLACE,count, k ,MPI_INT,op2,0,MPI_COMM_WORLD);

      #pragma omp parallel for private(i,j) shared(mean, sum, count)
			for (i = 0; i < k; i++) {
				for (j = 0; j < DIM; j++) {
					mean[i*DIM+j] = sum[i*DIM+j]/count[i];
				}
        //  printf("%5.2f, %5.2f, %5.2f \n", mean[i*DIM], mean[i*DIM+1], mean[i*DIM+2]);
			}
      //printf("\n\n");

		}else{

      MPI_Reduce(sum,sum,DIM * k,MPI_DOUBLE,op,0,MPI_COMM_WORLD);
      MPI_Reduce(count,count, k ,MPI_INT,op2,0,MPI_COMM_WORLD);

    }
	} //End of While

/*
 * Escritura de resultados por pantalla.
 * Si no se activa la variable DEBUG sólo se escriben los centroides.
 * Si se activa DEBUG se muestran todos los puntos.
 * Activar DEBUG sólo para depurar el programa.
 */

 if(myrank == 0){
   for (i = 0; i < k; i++) {
  		for (j = 0; j < DIM; j++)
  			printf("%5.2f ", mean[i*DIM+j]);
  		printf("\n");
  	}
 }


	#ifdef DEBUG
	for (i = 0; i < block; i++) {
		for (j = 0; j < DIM; j++)
			printf("%5.2f ", xx[i*DIM+j]);
		printf("%d\n", cluster[i]);
	}
	#endif

	MPI_Op_free( &op );
  MPI_Op_free( &op2 );
	MPI_Finalize();

	return(0);
}
