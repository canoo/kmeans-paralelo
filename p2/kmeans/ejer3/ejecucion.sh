#!/bin/bash
#SBATCH -J kmeans16-balanceador-debil
#SBATCH -o datosEjecucion16.out
#SBATCH  --ntasks 16
for i in $(seq 1 $SLURM_NNODES); do
    srun --exclusive -n 1 -N 1 sudo cpupower frequency-set --governor userspace > /dev/null & done
wait
for i in $(seq 1 $SLURM_NNODES); do
    srun --exclusive -n 1 -N 1 sudo cpupower frequency-set --freq 1.7G > /dev/null & done
wait

for i in $(seq 1 $SLURM_NNODES); do
    srun --exclusive -n 1 -N 1 cpupower frequency-info & done
wait
time prun ./a.out  input16.dat
