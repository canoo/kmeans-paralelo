#include <mpi.h>

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>

void InitVec (float *v, int dim)
{
	int i;

	for (i = 0; i < dim; i++) {
		v[i] = i+1;
//		v[i] = RAND;
	}
}

float DotProduct (float *a, float *b, int dim)
{
   int i;

   float dt = 0;

   for (i=0; i < dim; i++)
      dt += a[i] * b[i];
}

void EscribirVec (float *M, int dim, int rank)
{
	int i, j;

printf ("Proccess %d\n", rank);
	for (i=0; i < dim; i++)
	{
		printf ("%.1f ", M[i]);
	}

   printf ("\n");
}


void main (int argc, char *argv[]) {

  int myrank, size;

  int block = 1, dim=5, i;
  float *a, *b, dotproduct, dt;

  MPI_Status status;

  MPI_Init(&argc, &argv);                      
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank); 
  MPI_Comm_size(MPI_COMM_WORLD, &size);    

/* Lectura de parámetros de entrada */
/* Usage: ./MatVec */
   if (argc > 1) dim = atoi (argv[1]);

/*
 * Inicialización de los datos en el máster y distribución entre los procesos. 
 */
   if (myrank == 0)
   {
      a = (float *) malloc (dim * sizeof (float));
      b = (float *) malloc (dim * sizeof (float));

      InitVec (a, dim);
      InitVec (b, dim);

/* Se calcula el tamaño del bloque del vector que se envía a cada proceso */
      block = dim/size;

/* Envío de datos a todos los procesos salvo en Rank 0 */   
      for (i=1; i < (size); i++)
      {
        MPI_Send (&block, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        MPI_Send (&a[i*block], block, MPI_FLOAT, i, 0, MPI_COMM_WORLD);        
        MPI_Send (&b[i*block], block, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
      }
   }
/* Recepción de los datos en todos los procesos menos en Rank 0 */
   else
   {
      MPI_Recv (&block, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
      a = (float *) malloc (block * sizeof (float));
      b = (float *) malloc (block * sizeof (float));
      MPI_Recv (a, block, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, &status);
      MPI_Recv (b, block, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, &status);      
   }

/* 
 * Todos los procesos ejecutan la función de cálculo del producto escalar. 
 * Cada proceso lo hace con una parte del vector de datos original.
 */
  dotproduct = DotProduct (a, b, block);
  printf ("Process %d: dotrproduct = %f\n", myrank, dotproduct);
  fflush (stdout);

/* Todos los procesos envían su resultado parcial menos Rank 0
 * Inserta el código del envío aqui ---
 * ----
 */
if(myrank ==0){
	float *k =(float *) malloc(sizeof (float));
	for(i = 1; i <size; i++){	
		MPI_Recv(k,1,MPI_FLOAT,i,0,MPI_COMM_WORLD,&status);
		dotproduct = *(k) + dotproduct;
	
	}
	printf("Resultado: %f\n",dotproduct);
}else{
	MPI_Send(&dotproduct,1,MPI_FLOAT,0,0,MPI_COMM_WORLD);
}

MPI_Finalize();
exit(0);


/* El proceso Rank 0 recibe y acumula los resultados parciales 
 * Inserta el código de la recepción aquí ---


  if (myrank == 0) printf ("Resultado Final: %f\n", dotproduct);
  MPI_Finalize(); 
  exit (0);**/
}
