#include <mpi.h>

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>

#define RAND rand() % 100

void InitVec (float *v, int dim)
{
	int i;

	for (i = 0; i < dim; i++) {
		v[i] = i+1;
//		v[i] = RAND;
	}
}

float DotProduct (float *a, float *b, int dim)
{
   int i;

   float dt = 0;

   for (i=0; i < dim; i++)
      dt += a[i] * b[i];
}

void EscribirVec (float *M, int dim, int rank)
{
	int i, j;

printf ("Proccess %d\n", rank);
	for (i=0; i < dim; i++)
	{
		printf ("%.1f ", M[i]);
	}

   printf ("\n");
}

void main (int argc, char *argv[]) {

  int myrank, size;

  int block = 1, dim=5, i;
  float *a, *b, *aa, *bb, dotproduct, dt;

  MPI_Status status;

  MPI_Init(&argc, &argv);                      
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank); 
  MPI_Comm_size(MPI_COMM_WORLD, &size);    

/* Lectura de parámetros de entrada */
/* Usage: ./MatVec */
   if (argc > 1) dim = atoi (argv[1]);

//printf ("Parametros Entrada: nth=%d, block_size=%d, dim=%d\n", nthreads, block_size, dim);

   if (myrank == 0)
   {
      a = (float *) malloc (dim * sizeof (float));
      b = (float *) malloc (dim * sizeof (float));
      block = dim/size;
      InitVec (a, dim);
      InitVec (b, dim);
   }

   MPI_Bcast (&block, 1, MPI_INT, 0, MPI_COMM_WORLD);

   aa = (float *) malloc (block * sizeof (float));
   bb = (float *) malloc (block * sizeof (float));

   MPI_Scatter (a, block, MPI_FLOAT, aa, block, MPI_FLOAT, 0, MPI_COMM_WORLD);
   MPI_Scatter (b, block, MPI_FLOAT, bb, block, MPI_FLOAT, 0, MPI_COMM_WORLD);

//  EscribirVec (aa, block, myrank);
//  EscribirVec (bb, block, myrank); 
  dt = DotProduct (aa, bb, block);
  printf ("Process %d: dotrproduct = %f\n", myrank, dt);

/*
 * Introduce el código de recepción y cómputo de resultados aqui ---
 */

  float *f = (float *) malloc(sizeof(float)); //igual es mejor meterlo en myrank==0
  MPI_Reduce(&dt,f,size,MPI_FLOAT,MPI_SUM,0,MPI_COMM_WORLD);


  if (myrank == 0) printf ("Resultado Final: %f\n", *f);
  MPI_Finalize(); 
  exit (0);
}
