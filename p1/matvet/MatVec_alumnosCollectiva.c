#include <mpi.h>

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>

#define RAND rand() % 100

void InitMat (int dim, float *M)
{
	int i,j;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
				M[i*dim+j] = 1+j+i*dim;
//				M[i*dim+j] = RAND;
		}
	}
}

void InitVec (int dim, float *v)
{
	int i;

	for (i = 0; i < dim; i++) {
		v[i] = i+1;
//		v[i] = RAND;
	}
}

void MultiplicaMatVec (float *A, float *b, float *c, int dim)
{
	int i, j;

	for (i=0; i < dim; i++) {
     c[i] = 0.0;
		for (j=0; j < dim; j++)
			c[i] += A[j+dim*i] * b[j];
   }
}

void MultiplicaMatVecBlock (float *A, float *b, float *c, int dim, int block)
{
        int i, j;

        for (i=0; i < block; i++) {
     c[i] = 0.0;
                for (j=0; j < dim; j++)
                        c[i] += A[j+dim*i] * b[j];
   }
}


void EscribirMat (float *M, int rows, int cols)
{
	int i, j;

	for (i=0; i < rows; i++)
	{
		for (j=0; j < cols; j++)
			fprintf (stdout, "%.1f ", M[i*cols+j]);
		fprintf (stdout,"\n");
	}

   printf ("\n");
}

void EscribirVec (float *M, int dim, int num)
{
	int i, j;

	printf("proceso %d: ",num);
	for (i=0; i < dim; i++)
	{
		printf (" %.1f ",M[i]);
	}
	printf("\n");

   printf ("\n");
}

void main (int argc, char *argv[]) {

  int dim=4, i;
  float *A, *b, *c;

  int myrank,size;
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

/* Lectura de parámetros de entrada */
/* Usage: ./MatVec <dim>*/
   if (argc > 1) dim = atoi (argv[1]);

   int block = dim/size;

   if (myrank == 0)
   {
      A = (float *) malloc (dim * dim * sizeof (float));
      b = (float *) malloc (dim * sizeof (float));
      c = (float *) malloc (dim * sizeof (float));
      InitMat (dim, A);
      InitVec (dim, b);
      EscribirVec(A, dim*dim, -1);
      EscribirVec(b,dim,-1);


   }

  float *aa = (float *) malloc (dim * block * sizeof(float));
  float *bb = (float *) malloc (dim * sizeof(float));
  float *cc = (float *) malloc (block * sizeof(float));

  //int *disp = (int *) malloc(sizeof(int)); no hace falta el scatterv, el gather lo ordena
  // MPI_Cast


   MPI_Scatter(A,block*dim,MPI_FLOAT,aa,block*dim,MPI_FLOAT,0,MPI_COMM_WORLD);
   MPI_Bcast(b,dim,MPI_FLOAT,0,MPI_COMM_WORLD);

   MultiplicaMatVecBlock (aa, b, cc, dim,block);

   EscribirVec (cc, block, myrank);
  
   MPI_Gather(cc,block,MPI_FLOAT,c,block,MPI_FLOAT,0,MPI_COMM_WORLD);

   if(myrank == 0){
	printf("Resultado: \n");
	EscribirVec(c,dim,myrank);
}
  MPI_Finalize();
  exit (0);
}
